#Link to the problem: https://www.hackerrank.com/challenges/the-minion-game/problem?isFullScreen=true


#This breaks for time complexity reasons on larger inputs. I should implement a sliding window solution instead
def is_vowel(letter):
    return letter in ["A", "E", "I", "O", "U"]

def minion_game(string):
    const_dict = {}
    vowel_dict = {}
    
    for i in range(len(string)):
        for j in range(len(string), i, -1):
            sub = string[i:j]
            if is_vowel(sub[0]):
                if sub in vowel_dict:
                    vowel_dict[sub] += 1
                else:
                    vowel_dict[sub] = 1
            else:
                if sub in const_dict:
                    const_dict[sub] += 1
                else:
                    const_dict[sub] = 1
    
    stuart_score = sum(const_dict.values())          
    kevin_score =  sum(vowel_dict.values())    
        
    if stuart_score > kevin_score:
        print(f"Stuart {stuart_score}")
        return f"Stuart {stuart_score}"
    elif kevin_score > stuart_score:
        print(f"Kevin {kevin_score}")
        return f"Kevin {kevin_score}"
    else:
        print("Draw")
        return "Draw"

    
if __name__ == '__main__':